/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <walrus/XmlReader.hpp>
typedef enum
{
	GAME, MENU
} State;

Scene *readScene(std::string filename)
{
	Scene *scene;
	State state;
	std::string abs_filename = std::string(PATH)
			+ std::string("data/xml/scene/") + filename;
	TiXmlDocument doc(abs_filename.c_str());

	if (!doc.LoadFile())
	{
		std::cerr << "XML: Error loading file for " << filename << "\n";
		return NULL;
	}

	TiXmlHandle hDoc(&doc);

	TiXmlElement *pElem;
	pElem = hDoc.FirstChildElement().Element();

	if ((pElem == NULL) || !pElem)
	{
		std::cerr << "XML: Error with root of scene xml description\n";
		return NULL;
	}

	std::string type = pElem->Value();

	if (type == "Game")
	{
		state = GAME;
		scene = new GameEngine();
	}
	else if (type == "Menu")
	{
		state = MENU;
	}
	else
	{
		std::cout << "XML: Unknown type of Scene for xml parse, given : "
				<< type << "\n";
		return NULL;
	}

	if (state == GAME)
	{
		///Import physics
		if (!pElem->Attribute("physics"))
		{
			std::cerr << "XML: No physics found for GameEngine\n";
		}
		else
		{
			std::string physics = pElem->Attribute("physics");

			if (physics == "PBPhysics")
			{
				((GameEngine*) scene)->setPhysics(new PBPhysics());

			}
		}
		///Import Level
		TiXmlElement *pLevelElem = pElem->FirstChild("Level")->ToElement();

		if (!pLevelElem)
		{
			std::cerr
					<< "XML: Error loading level description in scene xml parsing\n";
			return NULL;
		}
		Level* level = readLevel(pLevelElem->FirstAttribute()->Value());
		if (!level)
		{
			std::cerr << "XML: Error loading level from readLevel\n";
		}

		((GameEngine*) scene)->setLevel(level);
		/** TODO in Menu
		 * This part will be manage by Menu, but before it we made here
		 */
		if(level != NULL)
			((GameEngine*) scene)->addPlayer(
						readCharacter(std::string("testPlayer.xml"),
								level->getSpawn()));
	}

	return scene;
}

Level *readLevel(std::string filename)
{
	Level *level = new Level();
	std::string abs_filename = std::string(PATH)
			+ std::string("data/xml/level/") + filename;

	TiXmlDocument doc(abs_filename.c_str());

	if (!doc.LoadFile())
	{
		std::cerr << "XML: Error loading file for " << filename << "\n";
		return NULL;
	}

	TiXmlHandle hDoc(&doc);

	TiXmlElement *pElem;
	pElem = hDoc.FirstChildElement().Element();

	if ((pElem == NULL) || !pElem)
	{
		std::cerr << "XML: Error with root of level xml description\n";
		return NULL;
	}

	std::string check = pElem->Value();

	if (!(check == "Level"))
	{
		std::cerr << "XML: Bad format level description level\n";
		delete level;
		return NULL;
	}
	int spawn_x = 0;
	int spawn_y = 0;
	pElem->QueryIntAttribute("spawn_x", &spawn_x);
	pElem->QueryIntAttribute("spawn_y", &spawn_y);
	level->setSpawn(Point(spawn_x, spawn_y));
	int gravity_x = 0;
	int gravity_y = 0;
	pElem->QueryIntAttribute("gravity_x", &gravity_x);
	pElem->QueryIntAttribute("gravity_y", &gravity_y);
	level->setGravity(Point(gravity_x, gravity_y));
	TiXmlElement *pLevelElem = pElem->FirstChild("GameObjects")->ToElement();

	if (!pLevelElem)
	{
		std::cerr << "XML: No GameObjects define\n";
		delete level;
		return NULL;
	}

	TiXmlElement *pObjectElem = pLevelElem->FirstChild()->ToElement();

	for (; pObjectElem; pObjectElem = pObjectElem->NextSiblingElement())
	{
		std::string type = pObjectElem->Value();

		if (type == "GameObject")
		{
			GameObject* object = NULL;
			std::string filename = pObjectElem->Attribute("filename");
			int px = 0;
			int py = 0;
			pObjectElem->QueryIntAttribute("pos_x", &px);
			pObjectElem->QueryIntAttribute("pos_y", &py);
			Point position = Point(px, py);
			int sx = 0;
			int sy = 0;
			pObjectElem->QueryIntAttribute("size_x", &sx);
			pObjectElem->QueryIntAttribute("size_y", &sy);
			Point size = Point(sx, sy);
			object = new GameObject(filename, position, size);
			///Search for box value
			int box = 0;
			pObjectElem->QueryIntAttribute("box", &box);
			if (box)
				object->setBox(Square(Point(0, 0), object->getSize()));
			///Side event managment
			bool ok = true;
			std::string gameType;
			if(pObjectElem->Attribute("type") != NULL)
			{
				gameType = pObjectElem->Attribute("type");
			} 
			else
			{
				ok = false;
			}
			if(ok && gameType == "solid")
			{
				///Set side event as solid
				object->setObjectType(walrus::object::SOLID);
			}
			else if(ok && gameType == "reflective")
			{
				///Set side event as reflective
				object->setObjectType(walrus::object::REFLECTIVE);
			}
			level->addObject(object);
		}
		else if (type == "NPChar")
		{
			///TODO Add a NPCharacter
		}
		else
		{
			std::cerr << "XML: Unknown GameObject type\n";
			delete level;
			return NULL;
		}
	}

	return level;
}

void retrieveImg(walrus::anim::State state,
		std::vector<std::string> *imgFilenames, int *imgIndex, int *index,
		TiXmlElement *pDirElem)
{
	TiXmlNode *pImgNode = pDirElem->FirstChild("Img1");

	if (pImgNode == NULL)
	{
		return;
	}

	TiXmlElement *pImgElem = pImgNode->ToElement();

	for (int i = 2; pImgNode != NULL; i++)
	{
		std::string imgFilename = pImgElem->Attribute("filename");
		imgFilenames->insert(imgFilenames->begin() + (*index), imgFilename);
		(*index)++;
		imgIndex[state]++;

		std::stringstream imgStringStream;
		std::string imgString;
		imgStringStream << "Img" << i;
		imgString = imgStringStream.str();
		pImgNode = pDirElem->FirstChild(imgString.c_str());

		if (pImgNode != NULL)
		{
			pImgElem = pImgNode->ToElement();
		}
	}
}

Character *readCharacter(std::string filename, Point position)
{
	std::string abs_filename = std::string(PATH)
			+ std::string("data/xml/character/") + filename;
	TiXmlDocument doc(abs_filename.c_str());

	if (!doc.LoadFile())
	{
		std::cerr << "XML: Error loading file for " << filename << "\n";
		return NULL;
	}

	TiXmlHandle hDoc(&doc);

	TiXmlElement *pElem;
	pElem = hDoc.FirstChildElement().Element();

	if ((pElem == NULL) || !pElem)
	{
		std::cerr << "XML: Error with root of player xml description\n";
		return NULL;
	}

	std::string check = pElem->Value();

	if (!(check == "Player"))
	{
		std::cerr << "XML: Unknown Player header, given : " << check << "\n";
		return NULL;
	}

	int sizeX = 0;
	int sizeY = 0;
	std::string newFilename = pElem->Attribute("filename");
	pElem->QueryIntAttribute("size_x", &sizeX);
	pElem->QueryIntAttribute("size_y", &sizeY);
	Character *player = NULL;

	if ((sizeX != 0) && (sizeY != 0))
	{
		player = new Character(newFilename, position, Point(sizeX, sizeY));
	}
	else
	{
		return player;
	}
	int box_o_x = 0;
	int box_o_y = 0;
	int box_e_x = 0;
	int box_e_y = 0;
	pElem->QueryIntAttribute("box_o_x", &box_o_x);
	pElem->QueryIntAttribute("box_o_y", &box_o_y);
	pElem->QueryIntAttribute("box_e_x", &box_e_x);
	pElem->QueryIntAttribute("box_e_y", &box_e_y);
	player->setBox(Square(Point(box_o_x, box_o_y), Point(box_e_x, box_e_y)));
	TiXmlElement *pStateElem = pElem->FirstChild("State")->ToElement();

	if (!pStateElem)
	{
		std::cerr << "XML: No State define\n";
		delete player;
		return NULL;
	}

	TiXmlElement *pObjectElem = pStateElem->FirstChild()->ToElement();
	int *imgIndex = NULL;
	imgIndex = (int *) calloc(walrus::anim::STATE, sizeof(int));

	if (imgIndex == NULL)
	{
		std::cerr << "Error while creating imgIndex for" << filename;
		delete player;
		return NULL;
	}

	memset(imgIndex, 0, walrus::anim::STATE);
	std::vector < std::string > imgFilenames;
	std::map<walrus::anim::State, int> speedMap;

	for (; pObjectElem; pObjectElem = pObjectElem->NextSiblingElement())
	{
		//Finish the player Xml Reading here
		walrus::anim::State state = walrus::anim::STILL;
		std::string type = pObjectElem->Value();

		if (type == "WALK")
		{
			state = walrus::anim::WALK;
		}
		else if (type == "RUN")
		{
			state = walrus::anim::RUN;
		}
		else if (type == "JUMP")
		{
			state = walrus::anim::JUMP;
		}
		else if (type == "DASH")
		{
			state = walrus::anim::DASH;
		}
		else
		{
			state = walrus::anim::STILL;
		}

		int speed = 0;
		pObjectElem->QueryIntAttribute("speed", &speed);
		speedMap[state] = speed;
		int index = 0;

		for (int i = 0; i < state; i++)
		{
			index += imgIndex[i];
		}
		///Import left images

		TiXmlNode *pLeftNode = pObjectElem->FirstChild("Left");

		if (pLeftNode == NULL)
		{
			break;
		}

		TiXmlElement *pLeftElem = pLeftNode->ToElement();
		retrieveImg(state, &imgFilenames, imgIndex, &index, pLeftElem);
		///Import right images
		TiXmlNode *pRightNode = pObjectElem->FirstChild("Right");

		if (pRightNode == NULL)
		{
			break;
		}

		TiXmlElement *pRightElem = pRightNode->ToElement();
		retrieveImg(state, &imgFilenames, imgIndex, &index, pRightElem);
	}

	player->setImgFilename(imgFilenames);
	player->setSpeedMap(speedMap);
	player->setImgIndex(imgIndex);

	///TODO Check if pair number of images
	return player;
}

int readConfig(std::string filename, std::map<int, int> *keyMap)
{
	filename = std::string(PATH) + std::string("data/xml/config/") + filename;
	TiXmlDocument doc(filename.c_str());

	if (!doc.LoadFile())
	{
		std::cerr << "Error loading file for config\n";
		return 1;
	}

	TiXmlHandle hDoc(&doc);
	TiXmlElement *pElem;
	TiXmlHandle hRoot(0);
	pElem = hDoc.FirstChildElement().Element();

	std::string check = pElem->Value();

	if (!(check == "Config"))
	{
		std::cerr << "xml: bad format config description config\n";

		return 1;
	}

	TiXmlElement *pKeyElem = pElem->FirstChild("Keys")->ToElement();

	if (!pKeyElem)
	{
		std::cerr << "XML: No KeyMap define\n";
		return 1;
	}

	TiXmlElement *pObjectElem = pKeyElem->FirstChild()->ToElement();

	for (; pObjectElem; pObjectElem = pObjectElem->NextSiblingElement())
	{
		int sfml = 0;
		int value = 0;
		pObjectElem->QueryIntAttribute("sfml", &sfml);
		pObjectElem->QueryIntAttribute("value", &value);

		//Put values into map
		(*keyMap)[sfml] = value;
	}

	return 0;
}

