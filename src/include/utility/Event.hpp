/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WALRUS_EVENT_HPP_
#define WALRUS_EVENT_HPP_
#include <walrus/const.hpp>
class GameObject;

namespace walrus
{
namespace event
{

class Event
{
public:
	Event(){time = -1.0;};
	~Event(){};
	virtual void execute(){};
	double getTime();
	void setTime(double);
private:
	double time;
};

template<class system> 
class SpecificEvent : public Event
{
public:
	SpecificEvent():walrus::event::Event(){};

	~SpecificEvent(){};
	void setFunction(system* mSystem, void (system::*function)(void), double time = -1.0)
	{
		this->mSystem = mSystem;
		this->function = function;
		this->setTime(time);
	};
	void execute()
	{
		if(function != NULL)
			(this->mSystem->*function)();		
	};
private:
	system* mSystem;
	void (system::*function)(void);
	std::string command;
};
} // namespace event

} // namespace walrus

#endif /* EVENT_HPP_ */
