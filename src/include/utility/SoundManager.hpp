#ifndef WALRUS_SOUNDMANAGER_HPP_
#define WALRUS_SOUNDMANAGER_HPP_

#include <walrus/const.hpp>
#include <SFML/Audio.hpp>
class SoundManager
{
private:
	sf::Music music;
	std::map<std::string, sf::SoundBuffer> sounds;

public:
	SoundManager();
	~SoundManager();
	void addSoundInBuffer(std::string);
	int setMusic(std::string);
};
#endif
