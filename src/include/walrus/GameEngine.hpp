/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef WALRUS_GAMEENGINE_H
#define WALRUS_GAMEENGINE_H
#include <walrus/Scene.hpp>
#include <walrus/const.hpp>
#include <walrus/Level.hpp>
#include <walrus/Physics.hpp>
#include <walrus/PBPhysics.hpp>
#include <walrus/Character.hpp>

/**
 * Scene used for game, contains some Physics management and a Level
 */
class GameEngine: public Scene
{
public:
	GameEngine();
	~GameEngine();

	int init(std::map<std::string, GLuint> *);

	void manageKey(bool *keys);
	void updateScene();
	void drawScene();

	void setLevel(Level *);
	void addPlayer(Player *);
	void setPhysics(Physics*);

	Level* getLevel();
	std::vector<Player*>* getPlayer();
	Physics* getPhysics();

	bool isGameEngine();

private:
	Physics *physics;
	Level *level;
	Point screenPos;
	int framerate;
	std::vector<Player *> players;
};
#endif //WALRUS_GAMEENGINE_H
