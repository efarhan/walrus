/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef WALRUS_SCENE_H
#define WALRUS_SCENE_H
#include <walrus/const.hpp>
#include <walrus/Point.hpp>
#include <utility/gltexture.h>
namespace walrus
{
namespace event
{
class Event;
}
}
namespace walrus
{
namespace scene
{
typedef enum
{
	UP, DOWN, LEFT, RIGHT, ACTION, SHOT, R, L, Z, SIZE
} Key;
}
}
/**
 * Abstract class for Scene management like GameEngine, Menu, etc...
 */
class Scene
{
private:
	Point* screenSize;
protected:
	std::vector<walrus::event::Event*> event;
	std::vector<walrus::event::Event*> newEvent;

public:
	Scene();
	virtual ~Scene();
	virtual void manageKey(bool *keys) = 0;
	virtual void updateScene();
	virtual void drawScene() = 0;

	virtual void setScreenSize(Point*);
	virtual GLuint importImg(std::string filename);
	virtual int init(std::map<std::string, GLuint> *) = 0;

	virtual bool isGameEngine() = 0;
	void addEvent(walrus::event::Event*);
};
#endif //WALRUS_SCENE_H
