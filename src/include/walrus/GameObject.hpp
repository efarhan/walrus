/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef WALRUS_GAMEOBJECT_H
#define WALRUS_GAMEOBJECT_H
#include <walrus/const.hpp>
#include <walrus/Point.hpp>
#include <walrus/Square.hpp>
#include <utility/Event.hpp>
/**
 * An image somewhere in the screen, moving but doesn't change the images
 */
namespace walrus
{
namespace object
{
typedef enum {NONE, SOLID, REFLECTIVE, OBJECTTYPE} ObjectType;
}
}
class GameObject
{
public:
	GameObject(std::string filename, Point position, Point size);
	virtual ~GameObject();
	void drawObject(Point);
	void setImg(GLuint);
	GLuint getImg(void);
	Point getPosition();
	void setPosition(Point);
	Point getSize();
	std::string getFilename();
	
	void setObjectType(walrus::object::ObjectType);
	walrus::object::ObjectType getObjectType();

	Square getBox(); //Get Box in object coordinate
	Square getWBox(); //Get Box in world cooridinate
	void setBox(Square);

	void setEvent(walrus::event::Event*);
	walrus::event::Event* getEvent();
	


protected:
	std::string filename;
	Point position; //in global coordinates
	Point size;
	GLuint img;
	Square box;
	walrus::object::ObjectType objectType;
	walrus::event::Event* event;
};
#endif //WALRUS_GAMEOBJECT_H
