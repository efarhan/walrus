/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef WALRUS_SFMLENGINE_H
#define WALRUS_SFMLENGINE_H
#include <walrus/const.hpp>

#include <SFML/Window.hpp>
#include <walrus/Engine.hpp>
#include <walrus/Scene.hpp>
#include <walrus/Point.hpp>

/**
 * child of Engine, use SFML2
 */
class SFMLEngine: public Engine
{
private:
	sf::Window *App;
	Scene *scene;
	Scene *scene2;
	std::map<int, int> keyMap;

public:
	SFMLEngine();
	~SFMLEngine(void);
	int init();
	void manageKeys(void);
	int run(void);
	int config(void);
};
#endif
