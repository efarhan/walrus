/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef WALRUS_POINT_H
#define WALRUS_POINT_H
#include <walrus/const.hpp>
#include <math.h>
class Square;
/**
 * Instead of writing std::pair, I better like having a class for Points
 */
class Point
{
private:
	int x;
	int y;

public:
	Point(int, int);
	~Point();
	int getX(void);
	int getY(void);
	void setX(int);
	void setY(int);

	Point operator +(Point);
	Square operator +(Square);
	Point operator -(Point);
	bool operator ==(Point);
	bool operator !=(Point);
	bool operator <(Point);
	bool operator >(Point);
	Point operator /(int);
	bool isIn(Square s);
	bool isIn(Point origin, Point end);
	double normalize();
};
namespace walrus
{
typedef Point Vector2;
}
#endif
