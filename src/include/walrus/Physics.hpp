/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef WALRUS_PHYSICS_H
#define WALRUS_PHYSICS_H

#include <walrus/const.hpp>
#include <walrus/Point.hpp>
#include <walrus/Character.hpp>
#include <math.h>

class GameEngine;
class Level;
namespace walrus
{
namespace physics
{
typedef enum
{
	NONE, RIGHT, LEFT, UP, DOWN, SIDE
} Side;
typedef enum { PLATFORMER, RPG } PhysicsType;
}
}
/**
 * Abstract Physics class
 */
class Physics
{

public:
	Physics();
	virtual ~Physics();
	virtual int init(GameEngine *, std::vector<Player*>*);
	virtual int manageKeys(bool* keys)=0;
	virtual int update() = 0;
	virtual walrus::physics::Side collisionSide(Square before, Square after,
			Square s);
	void setPhysicsType(walrus::physics::PhysicsType);
	walrus::physics::PhysicsType getPhysicsType();

protected:
	GameEngine* game;
	Level* level;
	std::vector<Player*>* players;
	walrus::physics::PhysicsType type;
};
/**
 * Define some functions for GameObject Event
 */
void solidCollisionUp    		(Player*, GameObject*);
void solidCollisionDown  		(Player*, GameObject*);
void solidCollisionRight 		(Player*, GameObject*);
void solidCollisionLeft  		(Player*, GameObject*);
void reflectiveCollisionLeft 	(Player*, GameObject*);
void reflectiveCollisionRight 	(Player*, GameObject*);
#endif
