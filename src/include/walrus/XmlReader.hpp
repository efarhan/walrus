/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef WALRUS_XMLREAD_H
#define WALRUS_XMLREAD_H
#include <walrus/const.hpp>
#include <utility/tinyxml.h>

#include <SFML/Window.hpp>

#include <walrus/Level.hpp>
#include <walrus/Scene.hpp>
#include <walrus/GameEngine.hpp>
#include <walrus/GameObject.hpp>
#include <walrus/Character.hpp>
#include <walrus/Physics.hpp>

Scene * readScene(std::string filename);
Level * readLevel(std::string filename);
Character *readCharacter(std::string filename, Point position);
int readConfig(std::string filename, std::map<int, int> *);

#endif
