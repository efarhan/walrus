#include <walrus/const.hpp>
#include <walrus/Level.hpp>
#include <walrus/Scene.hpp>
#include <walrus/Character.hpp>

int writeScene(Scene *scene);
int writeLevel(Level *level);
int writeCharacter(Character *player);
