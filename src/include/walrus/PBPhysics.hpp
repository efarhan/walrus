/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef WALRUS_PBPHYSICS_H
#define WALRUS_PBPHYSICS_H

#include <walrus/const.hpp>
#include <walrus/Point.hpp>
#include <walrus/Physics.hpp>
#include <walrus/Level.hpp>
#include <walrus/Character.hpp>
/**
 * Player-based physics, test the GameObject that are in a Square around
 * the player and also calculate the new position if there are 
 * collisions
 */
class PBPhysics: public Physics
{

public:
	PBPhysics();
	virtual ~PBPhysics();
	int init(GameEngine *, std::vector<Player*>*);
	int update();
	int manageKeys(bool* keys);
private:
	std::vector<bool> jumping;
};


#endif
