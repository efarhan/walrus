/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef WALRUS_NPCHAR_H
#define WALRUS_NPCHAR_H
#include <walrus/Point.hpp>
#include <walrus/GameObject.hpp>
#include <walrus/Animation.hpp>
/**
 * Used for moving GameObject like Player Character or Non-Player Character
 */
class Character: public GameObject
{
public:
	Character(std::string filename, Point position, Point size);
	virtual ~Character();
	void updateImage();
	void setImgIndex(int *);
	void setImgFilename(std::vector<std::string>);
	void setSpeedMap(std::map<walrus::anim::State, int>);

	void setState(walrus::anim::State);
	void setDirection(walrus::anim::Direction);

	Point getNewPos();
	void setNewPos(Point);
	Square getNewBox();

	Animation* getAnimation();

	std::vector<GLuint>* getImgs();
	int *getImgIndex();
	std::vector<std::string>* getImgFilename();
	std::map<walrus::anim::State, int>* getSpeedMap();

private:

	Point newPos;
	Animation *animation;
	int *imgIndex;
	std::vector<GLuint> imgs;
	std::vector<std::string> imgFilename;
	std::map<walrus::anim::State, int> speedMap;
};
typedef Character Player;
#endif //WALRUS_NPCHAR_H
