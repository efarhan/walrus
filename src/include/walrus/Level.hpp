/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WALRUS_LEVEL_H_
#define WALRUS_LEVEL_H_
#include <walrus/GameObject.hpp>

/***
 * Level descriptor, containing the GameObjects (or the NP-Char)
 */
class Level
{
public:
	Level();
	virtual ~Level();
	void setGravity(Point);
	Point getGravity();
	void addObject(GameObject *);

	void setSpawn(Point);
	Point getSpawn();

	std::vector<GameObject *> *getGameObjects();

private:
	Point gravity;
	std::vector<GameObject *> sceneObjects;
	Point spawn;
};

#endif /* LEVEL_H_ */
