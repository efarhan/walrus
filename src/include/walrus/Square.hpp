/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef WALRUS_SQUARE_H
#define WALRUS_SQUARE_H

#include <walrus/const.hpp>
class Point;
/**
 * A Square is two Points, can check if there are collisions
 */
class Square
{
public:
	Square(Point origin, Point end);
	~Square();
	Point getOrigin();
	Point getEnd();

	void setOrigin(Point);
	void setEnd(Point);
	bool isIn(Square);
	bool isIn(Point, Point);
	bool isIn(Point);
	bool isInX(int x);
	bool isInY(int y);
	void checkPoints();

	bool operator ==(Square);
	Square operator +(Square);
	Square operator +(Point);

private:
	Point origin;
	Point end;
};

#endif
