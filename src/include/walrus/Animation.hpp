/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef WALRUS_ANIMATION_H
#define WALRUS_ANIMATION_H
#include<walrus/const.hpp>
#include<walrus/Point.hpp>
class Character;
namespace walrus
{
namespace anim
{
typedef enum
{
	STILL, WALK, RUN, JUMP, DASH, STATE
} State;
typedef enum
{
	LEFT, RIGHT, UP, DOWN, DIRECTION
} Direction;
}
}
/**
 * Used by Character to manage the State and the Direction
 */
class Animation
{
public:
	Animation(Character *);
	~Animation();
	void setDirection(walrus::anim::Direction);
	void setState(walrus::anim::State);
	void setNewState(walrus::anim::State);

	walrus::anim::Direction getDirection();
	walrus::anim::State getState();
	walrus::anim::State getNewState();

	void newImage();

	Point getSpeed();
	void setSpeed(Point);

private:
	Point speed;
	walrus::anim::State state;
	walrus::anim::State newState;
	walrus::anim::Direction direction;
	Character *object;
	int imgIndex;
};

#endif
