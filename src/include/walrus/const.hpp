/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef WALRUS_CONST_H
#define WALRUS_CONST_H




#define PATH "./"

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iterator>
#include <string>
#include <sstream>
#include <map>
#include <stdexcept>
#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>

#else
#ifdef WIN32
#include <Windows.h>
#endif
#include <GL/gl.h>
#include <GL/glu.h>
#endif

//typedef enum { UP = 0, DOWN, RIGHT, LEFT, ABUTTON, BBUTTON, START, SELECT, R1, R2, UP2, DOWN2, RIGHT2, LEFT3 } Touch;

#define FPS 60
#define IMG_FRAME_RATE 6
//#define VERBOSE
//#define SHOW_KEYS
#define PLAYER_WINDOW 100
//#define SHOW_KEYS
//#define SHOW_FPS
#endif //WALRUS_CONST_H
