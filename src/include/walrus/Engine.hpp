/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef WALRUS_ENGINE_H
#define WALRUS_ENGINE_H
#include <walrus/const.hpp>
#include <walrus/Scene.hpp>
#include <walrus/Point.hpp>
#include <utility/gltexture.h>
#include <walrus/XmlReader.hpp>
class PythonInterface;
/**
 * Abstract class used for example for SDL or SFML or other libraries
 */
class Engine
{
protected:
	bool finish;
	Point screen_size;
	std::map<std::string, GLuint> imgs;
	Scene *scene1;
	Scene *scene2;
	Scene *currentScene;
	PythonInterface *python;

public:
	Engine();
	virtual ~Engine(void);
	virtual int init() = 0;
	virtual void manageKeys(void) = 0;
	virtual int run(void) = 0;
	virtual int config(void) = 0;
	virtual Point getScreenSize();

	Scene* getScene1();
	Scene* getScene2();
	PythonInterface* getPython();

	virtual void setScreenSize(Point);
	bool setScene1(std::string filename);
	bool setScene2(std::string filename);
	bool switchScene();
	int execute(std::string filename);
	void quit();
	
	void addEvent(walrus::event::Event*);
};
#endif
