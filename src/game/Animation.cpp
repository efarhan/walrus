/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <walrus/Animation.hpp>
#include <walrus/Character.hpp>
Animation::Animation(Character *object) :
		speed(Point(0, 0)), imgIndex(0)
{
	this->object = object;
	this->direction = walrus::anim::LEFT;
	this->state = walrus::anim::STILL;
	this->newState = walrus::anim::STILL;
}

Animation::~Animation()
{
}

void Animation::setState(walrus::anim::State state)
{
	this->state = state;
}

void Animation::setDirection(walrus::anim::Direction direction)
{
	this->direction = direction;
}

walrus::anim::State Animation::getState()
{
	return this->state;
}
walrus::anim::State Animation::getNewState()
{
	return newState;
}
void Animation::setNewState(walrus::anim::State newState)
{
	this->newState = newState;
}

walrus::anim::Direction Animation::getDirection()
{
	return this->direction;
}
void Animation::newImage()
{
	///Calculate the index
	int index = 0;
	for (int i = 0; i < state; i++)
	{
		index += object->getImgIndex()[i];
	}
	if (direction == walrus::anim::RIGHT)
	{
		index += object->getImgIndex()[state] / 2;
	}
	///Calculate the max-value
	int max = index + object->getImgIndex()[state] / 2;
	///New Image index
	if (imgIndex < index || imgIndex >= max - 1)
	{
		imgIndex = index;
	}
	else
	{
		imgIndex++;
	}
	///Set new image in Character
	object->setImg((*(object->getImgs()))[imgIndex]);
}
Point Animation::getSpeed()
{
	return speed;
}
void Animation::setSpeed(Point p)
{
	this->speed = p;
}

