/*
 * Level.cpp
 *
 *  Created on: Oct 11, 2012
 *      Author: efarhan
 */

#include <walrus/Level.hpp>

Level::Level() :
		gravity(Point(0, 0)), spawn(Point(0, 0))
{
}

Level::~Level()
{
	std::vector<GameObject *>::iterator object;

	for (object = sceneObjects.begin(); object != sceneObjects.end(); object++)
	{
		delete (*object);
	}
}

void Level::setGravity(Point gravity)
{
	this->gravity = gravity;
}
Point Level::getGravity()
{
	return gravity;
}

void Level::addObject(GameObject *object)
{
	this->sceneObjects.push_back(object);
}

std::vector<GameObject *> *Level::getGameObjects()
{
	return &sceneObjects;
}

void Level::setSpawn(Point spawn)
{
	this->spawn = spawn;
}
Point Level::getSpawn()
{
	return this->spawn;
}
