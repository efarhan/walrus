/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <walrus/GameEngine.hpp>

GameEngine::GameEngine() :
		physics(NULL), level(NULL), screenPos(0, 0), framerate(0)
{
	
}

GameEngine::~GameEngine()
{
	std::vector<Player*>::iterator playerIt;
	for(playerIt = players.begin(); playerIt != players.end(); playerIt++)
		delete *playerIt;
	delete physics;
	delete level;
}

void GameEngine::updateScene()
{
	if (physics != NULL)
	{
		physics->update();
	}
	///Update players images
	if (framerate < IMG_FRAME_RATE)
	{
		framerate++;
	}
	else
	{
		std::vector<Player*>::iterator itPlayer;
		for (itPlayer = players.begin(); itPlayer != players.end(); itPlayer++)
		{
			(*itPlayer)->updateImage();
		}
		framerate = 0;
	}
	Scene::updateScene();
}

void GameEngine::manageKey(bool *keys)
{
	if (physics != NULL)
	{
		physics->manageKeys(keys);
	}

}

void GameEngine::drawScene()
{
	std::vector<GameObject *>::iterator it;
	std::vector<GameObject *> *imgs = level->getGameObjects();

	for (it = imgs->begin(); it != imgs->end(); it++)
	{
		(*it)->drawObject(screenPos);
	}
	std::vector<Player*>::iterator itPlayer;
	for (itPlayer = players.begin(); itPlayer != players.end(); itPlayer++)
	{
		(*itPlayer)->drawObject(screenPos);
	}
}

void GameEngine::setLevel(Level *level)
{
	this->level = level;
	if (this->physics)
		this->physics->init(this, &players);
}

int GameEngine::init(std::map<std::string, GLuint> *imgsMap)
{
	///Import imgs for gameobject
	int status = 0;
	std::vector<GameObject *>::iterator it;
	std::vector<GameObject *> *imgs = level->getGameObjects();

	for (it = imgs->begin(); it != imgs->end(); it++)
	{
		std::map<std::string, GLuint>::iterator mapIt = imgsMap->find(
				(*it)->getFilename());

		if (mapIt == imgsMap->end())
		{
			(*it)->setImg(importImg((*it)->getFilename()));
			imgsMap->insert(
					std::pair<std::string, GLuint>((*it)->getFilename(),
							(*it)->getImg()));
		}
		else
		{
			(*it)->setImg((GLuint) mapIt->second);
		}

		if ((*it)->getImg() == 0)
		{
			status = 1;
		}
	}

	/*
	 * Used to check the map of imgs and the status value
	 */
#ifdef VERBOSE

	for (std::map<std::string, GLuint>::iterator i = imgsMap->begin();
			i != imgsMap->end(); i++)
	{
		std::cout << (*i).first << " => " << (*i).second << "\n";
	}

	std::cout << "Status value : " << status << "\n";
#endif

	/// Import Player imgs
	std::vector<Player*>::iterator player;
	for (player = players.begin(); player != players.end(); player++)
	{
		std::vector<std::string>::iterator imgFilename;
		for (imgFilename = (*player)->getImgFilename()->begin();
				imgFilename != (*player)->getImgFilename()->end();
				imgFilename++)
		{
			std::map<std::string, GLuint>::iterator mapIt = imgsMap->find(
					(*imgFilename));

			if (mapIt == imgsMap->end())
			{
				GLuint imgNum = importImg((*imgFilename));
				imgsMap->insert(
						std::pair<std::string, GLuint>((*imgFilename), imgNum));
				(*player)->getImgs()->push_back(imgNum);
			}
			else
			{
				(*player)->getImgs()->push_back((GLuint) mapIt->second);
			}
		}
	}
	///TODO Remove unused images from map
	return status;
}
void GameEngine::setPhysics(Physics* physics)
{
	this->physics = physics;
}

Level* GameEngine::getLevel()
{
	return level;
}
std::vector<Player*>* GameEngine::getPlayer()
{
	return &players;
}
Physics* GameEngine::getPhysics()
{
	return physics;
}
bool GameEngine::isGameEngine()
{
	return true;
}
void GameEngine::addPlayer(Player* player)
{
	players.push_back(player);
}
