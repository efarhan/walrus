/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <walrus/Character.hpp>
Character::Character(std::string filename, Point position, Point size) :
		GameObject(filename, position, size), newPos(Point(0, 0))
{
	animation = new Animation(this);
}

Character::~Character()
{
	delete animation;
	free(imgIndex);
}

void Character::setImgFilename(std::vector<std::string> imgFilename)
{
	this->imgFilename = imgFilename;
}

void Character::setSpeedMap(std::map<walrus::anim::State, int> speedMap)
{
	this->speedMap = speedMap;
}

void Character::setImgIndex(int *imgIndex)
{
	this->imgIndex = imgIndex;
}

int *Character::getImgIndex()
{
	return this->imgIndex;
}
std::vector<GLuint>* Character::getImgs()
{
	return &imgs;
}

std::vector<std::string>* Character::getImgFilename()
{
	return &imgFilename;
}

std::map<walrus::anim::State, int>* Character::getSpeedMap()
{
	return &speedMap;
}

void Character::setState(walrus::anim::State newState)
{
	/*
	 * If there are images
	 */
	if (imgIndex[newState])
		animation->setNewState(newState);
}

void Character::setDirection(walrus::anim::Direction direction)
{
	animation->setDirection(direction);
}
void Character::updateImage()
{
	animation->newImage();
}
Point Character::getNewPos()
{
	return newPos;
}
void Character::setNewPos(Point p)
{
	newPos = p;
}
Square Character::getNewBox()
{
	return box + newPos;
}
Animation* Character::getAnimation()
{
	return animation;
}

