/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <walrus/GameObject.hpp>
GameObject::GameObject(std::string filename, Point position, Point size) :
		 filename(filename), position(position), size(size), box(
				Square(Point(0, 0), Point(0, 0))), objectType(walrus::object::NONE)
{
	this->img = 0;
}

GameObject::~GameObject()
{
	//	glDeleteTextures(1, &img);
}

void GameObject::setImg(GLuint img)
{
	this->img = img;
}

GLuint GameObject::getImg()
{
	return this->img;
}

Point GameObject::getPosition()
{
	return position;
}
void GameObject::setPosition(Point p)
{
	position = p;
}
Point GameObject::getSize()
{
	return size;
}
std::string GameObject::getFilename()
{
	return filename;
}

/**
 * Draw object in OpenGL context, given the screenPosition in world-
 * coordinates
 */
void GameObject::drawObject(Point screenPosition)
{
	//It is done without using glQuads for compatibility with OpenGL ES
	if (img != 0)
	{
		GLfloat vertices[] =
		{ 0.0f, 0.0f, (GLfloat) this->size.getX(), 0.0f, 0.0f,
				(GLfloat) this->size.getY(), (GLfloat) this->size.getX(),
				(GLfloat) this->size.getY(), };

		GLfloat tex_indices[] =
		{ 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, };

		GLfloat colors[] =
		{ 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
				1.0f, 1.0f, 1.0f, 1.0f, 1.0f, };

		glBindTexture(GL_TEXTURE_2D, this->img);

		glEnableClientState (GL_VERTEX_ARRAY);
		glEnableClientState (GL_COLOR_ARRAY);
		glEnableClientState (GL_TEXTURE_COORD_ARRAY);

		glVertexPointer(2, GL_FLOAT, 0, vertices);
		glColorPointer(4, GL_FLOAT, 0, colors);
		glTexCoordPointer(2, GL_FLOAT, 0, tex_indices);

		glTranslatef(position.getX() - screenPosition.getX(),
				position.getY() - screenPosition.getY(), 0);

		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		glTranslatef(-(position.getX() - screenPosition.getX()),
				-(position.getY() - screenPosition.getY()), 0);
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	}
}
/**
 * Get Box in object-coordinates
 */
Square GameObject::getBox()
{
	return box;
}
/**
 * Get Box in world-coordinates
 */
Square GameObject::getWBox()
{
	return box + position;
}
/**
 * Set Box in object-coordinates
 */
void GameObject::setBox(Square newBox)
{
	this->box = newBox;
}
walrus::object::ObjectType GameObject::getObjectType()
{
	return this->objectType;
}
void GameObject::setObjectType(walrus::object::ObjectType type)
{
	this->objectType = type;
}
void GameObject::setEvent(walrus::event::Event* event)
{
	this->event = event;
}
walrus::event::Event* GameObject::getEvent()
{
	return this->event;
}