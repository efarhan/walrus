/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <walrus/Scene.hpp>
#include <utility/Event.hpp>
Scene::Scene() :
		screenSize(NULL)
{
}

Scene::~Scene()
{
}

void Scene::setScreenSize(Point* p)
{
	this->screenSize = p;
}

GLuint Scene::importImg(std::string filename)
{
	std::string full_filename = std::string(PATH)
			+ std::string("/data/sprites/") + filename;

	FILE *file = NULL;

	if ((file = fopen(full_filename.c_str(), "rb")) == NULL)
	{
		fprintf(stderr, "Error: %s file not found\n", full_filename.c_str());
		return 0;
	}

	GLuint image = GLLoadPng(file);
	fclose(file);
	return image;
}
void Scene::addEvent(walrus::event::Event* event)
{
	this->event.push_back(event);
}
void Scene::updateScene()
{
	while(!this->newEvent.empty())
	{
		event.push_back(newEvent.back());
		newEvent.pop_back();

	}
	while(!this->event.empty())
	{
		walrus::event::Event* tmp = this->event.back();
		if(tmp->getTime() < 0.0)
		{
			tmp->execute();
		}
		else
		{
			tmp->setTime(tmp->getTime()-(1.0/FPS));
			this->newEvent.push_back(tmp);
		}
		this->event.pop_back();
	}
}
