/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <walrus/SFMLEngine.hpp>
SFMLEngine::SFMLEngine() :
		Engine(), App(NULL)
{
	sf::VideoMode desktop = sf::VideoMode::getDesktopMode();
	std::cout << '(' << desktop.width << ',' << desktop.height << ")\n";

	this->screen_size = Point(800, 600);

}

SFMLEngine::~SFMLEngine()
{
	delete App;
}
int SFMLEngine::init()
{
	this->App = new sf::Window(
			sf::VideoMode(screen_size.getX(), screen_size.getY()), "Walrus");
	this->App->setVerticalSyncEnabled(true);
	this->App->setActive(true);
	this->finish = false;
	return 0;
}

int SFMLEngine::config(void)
{
	readConfig("default.xml", &keyMap);

	return 0;
}

void SFMLEngine::manageKeys()
{
	bool keys[walrus::scene::SIZE];
	memset(keys, 0, walrus::scene::SIZE);
	std::map<int, int>::iterator it;

	for (it = keyMap.begin(); it != keyMap.end(); it++)
	{
		keys[it->second] = keys[it->second]
				|| sf::Keyboard::isKeyPressed((sf::Keyboard::Key) it->first);
	}

	currentScene->manageKey(keys);
}

int SFMLEngine::run(void)
{
	glEnable (GL_TEXTURE_2D);
	glEnable (GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	sf::Clock clock;
#ifdef SHOW_FPS
	float lastTime = clock.getElapsedTime().asMilliseconds();
#endif

	App->setFramerateLimit(FPS);

	while (!finish)
	{
		// clear the buffers
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glViewport(0, 0, App->getSize().x, App->getSize().y);
		glMatrixMode (GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, App->getSize().x, App->getSize().y, 0, 1, -1);
		glMatrixMode (GL_MODELVIEW);
		glLoadIdentity();

		// check all the window's events that were triggered since the last iteration of the loop
		sf::Event event;

		while (App->pollEvent(event))
		{
			// "close requested" event: we close the window
			if (event.type == sf::Event::Closed)
			{
				finish = true;
			}

#ifdef SHOW_KEYS
			else if (event.type == sf::Event::KeyPressed)
			{
				std::cout << "EventKey :" << event.key.code << "\n";
			}
			else if (event.type == sf::Event::JoystickButtonPressed)
			{
				std::cout << "Joystick #" << event.joystickButton.joystickId
						<< " Button #" << event.joystickButton.button << "\n";
			}
			else if (event.type == sf::Event::JoystickMoved)
			{
				std::cout << "Joystick #" << event.joystickMove.joystickId
						<< " Axis #" << event.joystickMove.axis << " Position#"
						<< event.joystickMove.position << "\n";
			}
#endif
			break;
		}

		if (currentScene != NULL)
		{
			manageKeys();
			currentScene->updateScene();
			currentScene->drawScene();
		}

		App->display();
#ifdef SHOW_FPS

		//FPS information
		float currentTime = clock.getElapsedTime().asMilliseconds();

		float framerate = 1000 / (currentTime - lastTime);

		std::cout << framerate << '\n';

		lastTime = currentTime;
#endif
	}

	App->close();
	return 0;
}

