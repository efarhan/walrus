/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <walrus/Engine.hpp>
#include <utility/PythonInterface.hpp>
Engine::Engine() :
		finish(false), screen_size(0, 0), scene1(NULL), scene2(NULL), currentScene(
				NULL)
{
	python = new PythonInterface(this);
}

Engine::~Engine()
{
	delete scene1;
	delete scene2;
	delete python;
}

Point Engine::getScreenSize()
{
	return screen_size;
}

void Engine::setScreenSize(Point p)
{
	this->screen_size = p;
}

bool Engine::setScene1(std::string filename)
{
	scene1 = readScene(filename);

	if ((scene1 == NULL) || (scene1->init(&imgs) != 0))
	{
		std::cerr << "Scene: Error while init Scene from " << filename << "\n";
		delete scene1;
		scene1 = NULL;
	}

	return (scene1 != NULL);
}

bool Engine::setScene2(std::string filename)
{
	scene2 = readScene(filename);

	if (scene2->init(&imgs) != 0)
	{
		std::cerr << "Scene: Error while init Scene from " << filename << "\n";
		delete scene2;
		scene2 = NULL;
	}

	return (scene2 != NULL);
}

bool Engine::switchScene()
{
	if ((scene1 != NULL)
			&& ((currentScene == NULL) || (currentScene == scene2)))
	{
		currentScene = scene1;
	}
	else if (scene2 != NULL)
	{
		currentScene = scene2;
	}
	else if ((scene1 == NULL) && (scene2 == NULL))
	{
		std::cerr << "Warning: trying to switch with two NULL scenes\n";
		currentScene = NULL;
		return false;
	}

	return true;
}
int Engine::execute(std::string filename)
{
	return python->execute(filename);
}
Scene* Engine::getScene1()
{
	return scene1;
}
Scene* Engine::getScene2()
{
	return scene2;
}
PythonInterface* Engine::getPython()
{
	return python;
}
void Engine::quit()
{
	this->finish = true;
}
void Engine::addEvent(walrus::event::Event* event)
{
	if(currentScene != NULL)
		currentScene->addEvent(event);
}
