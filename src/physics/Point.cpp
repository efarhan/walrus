/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <walrus/Point.hpp>
#include <walrus/Square.hpp>

Point::Point(int x, int y)
{
	this->x = x;
	this->y = y;
}

Point::~Point()
{
}

int Point::getX()
{
	return this->x;
}

int Point::getY()
{
	return this->y;
}

void Point::setX(int x)
{
	this->x = x;
}

void Point::setY(int y)
{
	this->y = y;
}
Point Point::operator+(Point p)
{
	return Point(this->x + p.getX(), this->y + p.getY());
}
Square Point::operator+(Square s)
{
	return Square(s.getOrigin() + *(this), s.getEnd() + *(this));
}
Point Point::operator-(Point p)
{
	return Point(this->x - p.getX(), this->y - p.getY());
}

bool Point::operator==(Point p)
{
	return (this->getX() == p.getX()) && (this->getY() == p.getY());
}
bool Point::operator!=(Point p)
{
	return !((this->getX() == p.getX()) && (this->getY() == p.getY()));
}
bool Point::operator<(Point p)
{
	return (this->normalize() < p.normalize());
}
bool Point::operator>(Point p)
{
	return (this->normalize() > p.normalize());
}
Point Point::operator /(int d)
{
	return Point(x / d, y / d);
}
bool Point::isIn(Square s)
{
	return s.isIn(*this);
}

bool Point::isIn(Point origin, Point end)
{
	return Square(origin, end).isIn(*this);
}
double Point::normalize()
{
	return sqrt((double) this->x * this->x + this->y * this->y);
}
