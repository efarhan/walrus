/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <walrus/PBPhysics.hpp>
#include <walrus/GameEngine.hpp>
PBPhysics::PBPhysics() :
		Physics()
{
}

PBPhysics::~PBPhysics()
{
}
int PBPhysics::init(GameEngine* game, std::vector<Player*>* players)
{
	Physics::init(game, players);
	///Init jumping state controller
	std::cout<<"Init PBPhysics with players size: "<<players->size()<<"\n";
	this->jumping.resize(this->players->size(), false);

	return 0;
}
int PBPhysics::manageKeys(bool *keys)
{
#ifdef VERBOSE
	std::cout << "Keys :" << keys[0] << "," << keys[1] << "," << keys[2] << ","
			<< keys[3] << "," << keys[4] << "," << keys[5] << "\n";
#endif
	if(players->size() > jumping.size())
	{
		jumping.resize(players->size(), false);
	}
	///Translate the keys into players action
	try
	{
		players->at(0);
	} catch (std::out_of_range& oor)
	{
		std::cerr << "No Player: " << oor.what() << "\n";
		return 1;
	}
	Animation* animation = players->at(0)->getAnimation();
	int right = keys[walrus::scene::RIGHT] - keys[walrus::scene::LEFT];
	int up = keys[walrus::scene::UP] -keys[walrus::scene::DOWN];

	///JUMP
	animation->setNewState(walrus::anim::WALK);
	if(type == walrus::physics::PLATFORMER)
	{
	if (keys[walrus::scene::UP])
	{
		if(!this->jumping.at(0))
		{
			animation->setNewState(walrus::anim::JUMP);
			this->jumping.at(0) = true;
		}
	}
	else
	{
		try
		{
			this->jumping.at(0) = false;
		}
		catch (std::out_of_range& oor)
		{
			std::cerr << "No Jumping stated: " << oor.what() << "\n";
		}
	}
	}
	///Direction management
	///TODO also for RPG pyhsics type
	if (right == 1)
	{
		animation->setDirection(walrus::anim::RIGHT);
	}
	else if (right == -1)
	{
		animation->setDirection(walrus::anim::LEFT);
	}
	else
	{
		if (animation->getNewState() != walrus::anim::JUMP)
		{
			animation->setNewState(walrus::anim::STILL);
		}
	}
	if (animation->getNewState() != walrus::anim::STILL
			&& animation->getNewState() != walrus::anim::JUMP)
	{
		if (keys[walrus::scene::ACTION])
		{
			animation->setNewState(walrus::anim::RUN);
		}
		else if (keys[walrus::scene::SHOT])
		{
			animation->setNewState(walrus::anim::DASH);
		}
		else
		{
			animation->setNewState(walrus::anim::WALK);
		}

	}
	return 0;
}

int PBPhysics::update()
{
	std::vector<GameObject*>* objects = level->getGameObjects();
	std::vector<Player*>::iterator player;
	///Calculate for each player
	for (player = players->begin(); player != players->end(); player++)
	{
		///Calculate state and speed
		bool jumping = true;
		Animation* playerAnimation = (*player)->getAnimation();
		walrus::anim::State state = playerAnimation->getState();
		walrus::anim::State newState = playerAnimation->getNewState();
		switch (state)
		{
		case walrus::anim::JUMP:
			playerAnimation->setSpeed(
					Point(
							playerAnimation->getSpeed().getX(),
							playerAnimation->getSpeed().getY()));
			break;
		case walrus::anim::STILL:
		case walrus::anim::WALK:
		case walrus::anim::RUN:
		{
			playerAnimation->setState(newState);
			int speed = (*((*player)->getSpeedMap()))[newState];
			switch (newState)
			{
			case walrus::anim::JUMP:
			{
				playerAnimation->setSpeed(
						Point(playerAnimation->getSpeed().getX(), -speed));
				break;
			}
			default:
			{
				switch (playerAnimation->getDirection())
				{
				case walrus::anim::RIGHT:
				{
					playerAnimation->setSpeed(Point(speed, 0));
					break;
				}
				case walrus::anim::LEFT:
				{
					playerAnimation->setSpeed(Point(-speed, 0));
					break;
				}
				default:
					break;
				}

				break;
			}
			}
			break;
		}
		default:
			break;
		}

		///Calculate newPos of player
		playerAnimation->setSpeed(Point(playerAnimation->getSpeed().getX()+level->getGravity().getX(),
										playerAnimation->getSpeed().getY()+level->getGravity().getY()));
		(*player)->setNewPos(
				(*player)->getPosition() + playerAnimation->getSpeed());
		Point center = (*player)->getPosition() + ((*player)->getSize() / 2);
		Square window = Square(center - Point(PLAYER_WINDOW, PLAYER_WINDOW),
				center + Point(PLAYER_WINDOW, PLAYER_WINDOW));
		std::vector<GameObject*>::iterator objectIn;
		for (objectIn = objects->begin(); objectIn != objects->end();
				objectIn++)
		{
			Square objectBox = (*objectIn)->getWBox();
#ifdef VERBOSE
			std::cout << "Test for GameObjects: In window: "
					<< objectBox.isIn(window) << " Player in: "
					<< (*player)->getNewBox().isIn(objectBox)
					<< " Is there even a box: "
					<< (objectBox.getOrigin() != objectBox.getEnd()) << "\n";
#endif
			if (objectBox.getOrigin() != objectBox.getEnd()
					&& objectBox.isIn(window)
					&& (*player)->getNewBox().isIn(objectBox))
			{
				///Manage collision Player=>GameObject

				walrus::physics::Side side = collisionSide((*player)->getWBox(),
						(*player)->getNewBox(), objectBox);
#ifdef VERBOSE
				std::cout << "Collision! Object: Side:" << side << " ("
						<< objectBox.getOrigin().getX() << " "
						<< objectBox.getOrigin().getY() << ") ("
						<< objectBox.getEnd().getX() << " "
						<< objectBox.getEnd().getY() << ") Player: ("
						<< (*player)->getNewBox().getOrigin().getX() << " "
						<< (*player)->getNewBox().getOrigin().getY() << ") ("
						<< (*player)->getNewBox().getEnd().getX() << " "
						<< (*player)->getNewBox().getEnd().getY() << ")\n";
#endif
				switch (side)
				{
				case walrus::physics::DOWN:
					solidCollisionDown(*player, *objectIn);
					break;
				case walrus::physics::UP:
					solidCollisionUp(*player, *objectIn);
					jumping = false;
					break;
				case walrus::physics::RIGHT:
					if((*objectIn)->getObjectType() == walrus::object::SOLID)
					{
						solidCollisionRight(*player, *objectIn);
					}
					else if ((*objectIn)->getObjectType() == walrus::object::REFLECTIVE)
					{
						reflectiveCollisionRight(*player, *objectIn);
					}
					break;
				case walrus::physics::LEFT:
					if((*objectIn)->getObjectType() == walrus::object::SOLID)
					{
						solidCollisionLeft(*player, *objectIn);
					}
					else if ((*objectIn)->getObjectType() == walrus::object::REFLECTIVE)
					{
						reflectiveCollisionLeft(*player, *objectIn);
					}
					break;
				default:
					break;
				}
			}
		}
#ifdef VERBOSE
		std::cout << "Player position: " << (*player)->getPosition().getX()
				<< " " << (*player)->getPosition().getY() << " to "
				<< (*player)->getNewPos().getX() << " "
				<< (*player)->getNewPos().getY() << "\n";
#endif
		(*player)->setPosition((*player)->getNewPos());
		if (jumping)
		{
			playerAnimation->setState(walrus::anim::JUMP);
		}
	}
	return 0;
}

