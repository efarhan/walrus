/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <walrus/Physics.hpp>
#include <walrus/GameEngine.hpp>
#include <walrus/Physics.hpp>
Physics::Physics() :
level(NULL), players(NULL), type(walrus::physics::PLATFORMER)
{
}

Physics::~Physics()
{

}

int Physics::init(GameEngine* game, std::vector<Player*>* players)
{
	this->game = game;
	this->level = game->getLevel();
	this->players = players;
	return 0;
}
walrus::physics::PhysicsType Physics::getPhysicsType()
{
	return type;
}
void Physics::setPhysicsType(walrus::physics::PhysicsType type)
{
	this->type = type;
}
walrus::physics::Side Physics::collisionSide(Square before, Square after,
		Square s)
{
	Point diff = after.getOrigin() - before.getOrigin();
	double ratio = 0;
	int m = diff.getX();
	int n = diff.getY();
	if (n != 0 && m != 0)
		ratio = ((double) n) / m;
	else if (diff.getY() == 0)
		ratio = 1000;
	if (m > 0)
	{
		int left = s.getOrigin().getX() - before.getEnd().getX();
		if (n < 0)
		{

			double down = (s.getEnd().getY() - before.getOrigin().getY())
					/ ratio;
			if (left > 0 && (down < 0 || abs(left) < fabs(down)))
				return walrus::physics::LEFT;
			else
				return walrus::physics::DOWN;
		}
		else if (n > 0)
		{
			double up = (s.getOrigin().getY() - before.getEnd().getY()) / ratio;
			if (left > 0 && (up < 0 || abs(left) < fabs(up)))
				return walrus::physics::LEFT;
			else
				return walrus::physics::UP;
		}
		else
			return walrus::physics::LEFT;
	}
	else if (m < 0)
	{
		int right = s.getEnd().getX() - before.getOrigin().getX();
		if (n < 0)
		{
			double down = (s.getEnd().getY() - before.getOrigin().getY())
					/ ratio;
			if (down > 0 || (right < 0 && abs(right) + 2 < fabs(down)))
				return walrus::physics::RIGHT;
			else
				return walrus::physics::DOWN;
		}
		else if (n > 0)
		{
			double up = (s.getOrigin().getY() - before.getEnd().getY()) / ratio;
			if (up > 0 || (right < 0 && abs(right) + 2 < fabs(up)))
			{
				return walrus::physics::RIGHT;
			}
			else
				return walrus::physics::UP;
		}
		else
		{
			return walrus::physics::RIGHT;
		}
	}
	else
	{
		if (n > 0)
			return walrus::physics::UP;
		else if (n < 0)
			return walrus::physics::DOWN;
		else
			return walrus::physics::NONE;
	}
	return walrus::physics::NONE;

}
void solidCollisionDown(Player* player, GameObject* object)
{
	/**
	 * Collision down, it means that the player should be under, means newPos Y down object
	 * newPos=(newPos.x, object.end.y)
	 */
	player->getAnimation()->setSpeed(
			Point(player->getAnimation()->getSpeed().getX(), 0));
	player->setNewPos(
			Point(player->getNewPos().getX(),
					object->getWBox().getEnd().getY() + 1));
}
void solidCollisionUp(Player* player, GameObject* object)
{
	/**
	 * Collision up, player should be up the object, means newPos Y up object origin
	 * newPos=(newPos.x, object.origin.y)
	 */
	player->getAnimation()->setSpeed(
			Point(player->getAnimation()->getSpeed().getX(), 0));
	player->setNewPos(
			Point(player->getNewPos().getX(),
					object->getWBox().getOrigin().getY()
							- player->getBox().getEnd().getY() - 1));
	///TODO Complete the stop-jump
	if (player->getAnimation()->getState() == walrus::anim::JUMP)
	{
		if (player->getAnimation()->getSpeed().getX() == 0)
			player->getAnimation()->setState(walrus::anim::STILL);
		else
			player->getAnimation()->setState(walrus::anim::WALK);
	}
}
void solidCollisionLeft(Player* player, GameObject* object)
{
	/**
	 * Collision left, player should be left the object, means newPos X left object origin
	 * newPos=(object.origin.x, newPos.y)
	 */
	player->getAnimation()->setSpeed(
			Point(0, player->getAnimation()->getSpeed().getY()));
	player->setNewPos(
			Point(
					object->getWBox().getOrigin().getX()
							- player->getBox().getEnd().getX() - 1,
					player->getNewPos().getY() - 1));
}
void solidCollisionRight(Player* player, GameObject* object)
{
	/**
	 * Collision right, player should be right the object, means newPos X right object end
	 * newPos=(object.end.x, newPos.y)
	 */
	player->getAnimation()->setSpeed(
			Point(0, player->getAnimation()->getSpeed().getY()));
	player->setNewPos(
			Point(object->getWBox().getEnd().getX() + 1,
					player->getNewPos().getY() - 1));
}

void reflectiveCollisionRight(Player* player, GameObject* object)
{
	/**
	 * Reflective collision right
	 */
	 player->getAnimation()->setSpeed(
	 Point(-player->getAnimation()->getSpeed().getX(), player->getAnimation()->getSpeed().getY()));
	 player->setNewPos(
			Point(2*object->getWBox().getEnd().getX()-player->getNewBox().getOrigin().getX(),
					player->getNewPos().getY()));
}
void reflectiveCollisionLeft(Player* player, GameObject* object)
{
	/**
	 * Reflective collision left
	 */
	 player->getAnimation()->setSpeed(
	 Point(-player->getAnimation()->getSpeed().getX(), player->getAnimation()->getSpeed().getY()));
	 player->setNewPos(
			Point(2*object->getWBox().getOrigin().getX()-player->getNewBox().getEnd().getX(),
					player->getNewPos().getY()));
}
