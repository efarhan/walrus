/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <walrus/Point.hpp>
#include <walrus/Square.hpp>
Square::Square(Point newX, Point newY) :
		origin(newX), end(newY)
{
	this->checkPoints();
}

Square::~Square()
{
}

Point Square::getOrigin()
{
	return (this->origin);
}

Point Square::getEnd()
{
	return (this->end);
}

void Square::setOrigin(Point newX)
{
	this->origin = newX;
	checkPoints();
}

void Square::setEnd(Point newY)
{
	this->end = newY;
	checkPoints();
}

bool Square::isIn(Square other)
{
	bool status[] =
	{ (this->origin.getX() > other.getEnd().getX())
			|| (this->end.getX() < other.getOrigin().getX()),
			(this->origin.getY() > other.getEnd().getY())
					|| (this->end.getY() < other.getOrigin().getY()) };
	return !(status[0] || status[1]);
}

bool Square::isIn(Point x, Point y)
{
	return (this->isIn(Square(x, y)));
}

bool Square::isIn(Point p)
{
	return this->isInX(p.getX()) && this->isInY(p.getY());
}

void Square::checkPoints()
{
	//To make sure that the origin and the end are placed correctly
	if (origin.getX() > end.getX())
	{
		int tmp = origin.getX();
		origin.setX(end.getX());
		end.setX(tmp);
	}

	if (origin.getY() > end.getY())
	{
		int tmp = origin.getY();
		origin.setY(end.getY());
		end.setY(tmp);
	}
}

bool Square::isInX(int x)
{
	return (this->origin.getX() < x) && (this->end.getX() > x);
}

bool Square::isInY(int y)
{
	return (this->origin.getY() < y) && (this->end.getY() > y);
}

bool Square::operator==(Square s2)
{
	return (this->origin == s2.origin) && (this->end == s2.end);
}
Square Square::operator +(Square s2)
{
	return Square(this->origin + s2.getOrigin(), this->end + s2.getEnd());
}
Square Square::operator +(Point p)
{
	return Square(this->origin + p, this->end + p);
}

