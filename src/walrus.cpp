/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <walrus/const.hpp>
#include <walrus/Engine.hpp>
#include <walrus/SFMLEngine.hpp>
int main(int argc, char *argv[])
{
	(void) argc;
	(void) argv;

	Engine *engine = new SFMLEngine();
	engine->init();
	engine->config();

	if (!engine->setScene1("testGame1.xml"))
	{
		return EXIT_FAILURE;
	}

	engine->switchScene();
	engine->run();
	delete (engine);

	return EXIT_SUCCESS;
}

