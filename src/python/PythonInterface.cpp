/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <utility/PythonInterface.hpp>
#include <walrus/Engine.hpp>
/* --------------------------------------------------------------------
 * Test methods
 */
static PythonInterface* python = NULL;
/**
 * Test the correctness of the C-Python Interface by printing something
 */
static PyObject *testPython(PyObject *self, PyObject *args)
{
	(void) self;
	(void) args;
	std::cout << "Python interface works correctly!\n";
	return Py_BuildValue("i", 0);
}
/**
 * Return the string value of the Engine pointer
 */
static PyObject *getEngine(PyObject *self, PyObject *args)
{
	(void) self;
	(void) args;
	char enginePointer[30];
	if (python)
	{
		sprintf(enginePointer, "%p", python->getEngine());
	}
	return Py_BuildValue("s", enginePointer);
}
/* --------------------------------------------------------------------
 * Game interaction method
 */
static PyObject *getGameObjects(PyObject *self, PyObject *args)
{
	(void) self;
	(void) args;
	/*
	 * Check if the main scene of the engine is a GameEngine
	 */
	if (python->getEngine() == NULL || python->getEngine()->getScene1() == NULL
			|| !python->getEngine()->getScene1()->isGameEngine())
	{
		return Py_BuildValue("[]", NULL);
	}
	/* TODO convert the vector of GameObject into a array of string 
	 * containing the pointer value
	 */
	return NULL;
}
static PyObject *getPlayers(PyObject *self, PyObject *args)
{
	(void) self;
	(void) args;
	/*
	 * Check if the main scene of the engine is a GameEngine
	 */
	if (python->getEngine() == NULL || python->getEngine()->getScene1() == NULL
			|| !python->getEngine()->getScene1()->isGameEngine())
	{
		return Py_BuildValue("[]", NULL);
	}
	/* TODO convert the vector of GameObject into a array of string 
	 * containing the pointer value
	 */
	return NULL;
}
static PyMethodDef gameMethods[] =
{
{ "testPython", testPython, METH_VARARGS, "Just to test the python interface" },
{ "getEngine", getEngine, METH_NOARGS, "Get the engine pointer" },
{ "getEPlayers", getPlayers, METH_NOARGS, "Get the Players list" },
{ "getGameObjects", getGameObjects, METH_NOARGS, "Get the GameObjects list" },
{ NULL, NULL, 0, NULL } };

#if PY_MAJOR_VERSION == 3
static PyModuleDef gameModule =
{	PyModuleDef_HEAD_INIT, "walrus", NULL, -1, gameMethods,
	NULL, NULL, NULL, NULL};

static PyObject *PyInit_walrus(void)
{
	return PyModule_Create(&gameModule);
}

#endif

PythonInterface::PythonInterface(Engine *engine) :
		engine(engine)
{
	python = this;
}

PythonInterface::~PythonInterface()
{
	Py_Finalize();
}

int PythonInterface::init()
{
#if PY_MAJOR_VERSION == 2
	Py_Initialize();
	Py_InitModule("walrus", gameMethods);
#elif PY_MAJOR_VERSION == 3

	if (PyImport_AppendInittab("walrus", &PyInit_walrus) == -1)
	{
		return 1;
	}

	Py_Initialize();
#else
	std::cerr << "Error while init python, do you have python 2 or 3 ?\n";
	return 1;

#endif

	return 0;
}

int PythonInterface::execute(std::string filename)
{
	FILE *script = NULL;

	if ((script =
			fopen(
					(std::string(PATH) + std::string("data/script/") + filename).c_str(),
					"r")) == NULL)
	{
		std::cerr << "Error while loading script\n";
		return 1;
	}

	PyRun_SimpleFile(script,
			(std::string(PATH) + std::string("data/script/") + filename).c_str());
	fclose(script);
	return 0;
}
Engine* PythonInterface::getEngine()
{
	return this->engine;
}
