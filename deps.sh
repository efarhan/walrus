#!/bin/sh

if [ -d SFML -a ! -d SFML/.git ]; then
	rm -fr SFML
fi

if [ ! -d SFML ]; then
	git clone git://github.com/LaurentGomila/SFML.git
else
	cd SFML
	git pull
	cd ..
fi

cd SFML
cmake .
make
cd ..

if [ -d Box2D -a ! -d Box2D/.svn ]; then
	rm -fr Box2D
fi

if [ -d Box2D ]; then
	svn update Box2D
else
	svn checkout http://box2d.googlecode.com/svn/trunk/Box2D Box2D
fi

cd Box2D

sed -i '' 's/BOX2D_INSTALL_BY_DEFAULT ON/BOX2D_INSTALL_BY_DEFAULT OFF/g' CMakeLists.txt
sed -i '' 's/"Build Box2D examples" ON/"Build Box2D examples" OFF/g' CMakeLists.txt

cmake .
make

cd ..
