#Walrus
Platformer 2D, written in C++, cross-platform (maybe on mobile platform)
##Project depends on :

-CppUnit for unit-test,

-SFML 2.0,

-libpng (with zlib)

-OpenGL

-Python (2 or 3, whatever, maybe tinypy)

(-Box2D)

Project use SFML 2.0, download the git repository and compile it using CMake.



If you want to compile the program on windows (well, good luck), you have to use python33. It works! I tested it, so don't use anything else! Description are detailed in the INSTALL file.



#TODO:

##Windows:
-Make it work clearly [CLEARED]
	
-Complete the cmake_modules for cppunit such that it tells if cppunit were not in the path

##Generic input configuration:

-Design [CLEARED]

-Implement keyboard keys [CLEARED]

-Implement several joysticks keys

(-Implement mouse control)

##Player:

-Design [CLEARED]

-Finish XmlReader for player, refactor the header of each functions, complete NPChar with the img data [CLEARED]

-Implement player animation, keys => action [CLEARED]

##Physics engine:

-Collision-detection [CLEARED]

-Gravity[CLEARED]

-Movement[CLEARED]

##Editor:

-UI

-Keys mapping

##Python interface:

-Catch the GameObject

-Interact with them

(-tinypy implementation)

##Event managment:

-Design [CLEARED]

-Implement with python interface

