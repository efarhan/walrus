/*
 * XmlEditor.hpp
 *
 *  Created on: Oct 28, 2012
 *      Author: efarhan
 */

#ifndef XMLEDITOR_HPP_
#define XMLEDITOR_HPP_
#include "EditorScene.hpp"
class XmlEditor
{
public:
	XmlEditor();
	virtual ~XmlEditor();
	int writeEditor(EditorScene*);
	EditorScene* readEditor(std::string);
};

#endif /* XMLEDITOR_HPP_ */
