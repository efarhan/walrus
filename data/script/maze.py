#!/usr/bin/python
'''(c) 2012-2013 Elias Farhan
This module can be used in Blender 2.6. It was done for the Computer Graphics course at EPFL (Switzerland)'''
import sys
blender = True
walrus = True
try:
	import bpy
	import mathutils
except ImportError:
	sys.stderr.write("You're not in blender\n")
	blender = False
try:
	import walrus
except ImportError:
	sys.stderr.write("You're not in walrus\n")
	walrus = False
import math
from random import randint
width = 5
height = 10
wallX = [] #size width
wallY = [] #size height
ground = []
maze = []
d3 = 2
R = 2
wallH = 1
verbose = True
sys.setrecursionlimit(1000)
def destroyWall(position, newPos):
	'''Destroy the wall between two positions for maze generation,
	Modify the wallX, wallY list'''
	(pX, pY) = position
	(x, y) = newPos
	if(verbose):
		sys.stdout.write(str(position)+"->"+str(newPos))
	global wallX, wallY, width, height
	if ((x-pX+width)%width == 1 and (y-pY)==0):
		wallX[x][y] = False
	elif (((x-pX+width)%width == width-1 and (y-pY)==0)):
		wallX[pX][pY] = False
	elif ((y-pY+height)%height == 1 and (x-pX) == 0):
		wallY[y][x] = False
	elif ((y-pY+height)%height == height-1 and (x-pX)==0):
		wallY[pY][pX] = False
	else:
		sys.stderr.write('Error on destroyWall\n')
def DFS(position):
	'''Depth-First Search algorithms used recursively
	Modify ground, wallX, wallY'''
	(pX, pY) = position
	global ground, width, height, d3
	ground[pX][pY] = True
	neighbors = []
	for i in [-1, 1]:
		if(d3 == 1):
			x = (pX+i+width)%width
			y = (pY+i+height)%height
			if (not ground[x][pY]):
				neighbors.append((x, pY))
			if (not ground[pX][y]):
				neighbors.append((pX, y))
		elif(d3 == 2):
			x = (pX+i+width)%width
			y = pY+i
			if(not ground[x][pY]):
				neighbors.append((x, pY))
			if(y >= 0 and y < height and not ground[pX][y]):
                                neighbors.append((pX, y))
		else:
			x = pX+i
			y = pY+i
			if(x >= 0 and x < width and not ground[x][pY]):
				neighbors.append((x, pY))
			if(y >= 0 and y < height and not ground[pX][y]):
				neighbors.append((pX, y))
	l = len(neighbors)
	while(l != 0):
		(x, y) = neighbors.pop(randint(0, l-1))
		if(not ground[x][y]):
			destroyWall((pX, pY), (x, y))
			DFS((x, y))
		l = len(neighbors)
		
def generateMaze(size):
	'''Generate using the algorithm defined (default: DFS)
	Reset ground, wallX, wallY and width'''
	(w, h) = size
	global wallX, wallY, ground, width, height
	width = w
	height = h
	wallX = [[True]*height for x in range(0,width)]
	wallY = [[True]*width for x in range(0, height)] 
	ground = [[False]*(2*height-1) for x in range(0, 2*width-1)]
	DFS((0, 0))
	wallY[0][0] = False
def fillMaze():
	global width, height, wallX, wallY, maze
	maze = [[' ']*(2*width+1) for x in range(0, (2*height+1))]
	for j in range(0, height):
		for i in range(0, width):
			maze[2*j][2*i] = '+'
			if(wallX[i][j]):
				maze[2*j+1][2*i] = '|'
			if(wallY[j][i]):
				maze[2*j][2*i+1] = '-'
			maze[2*j][width-1] = '+'
		maze[2*j][2*width] = '+'
		if(wallX[0][j]):
			maze[2*j+1][2*width] = '|'
	for i in range(0, width):
		maze[2*height][2*i] = '+'
		if (wallY[0][i]):
			maze[2*height][2*i+1] = '-'
		else:
			maze[2*height][2*i+1] = ' '
	maze[2*height][2*width] = '+'
			
def printMaze(size):
	global width, height, maze
	(width, height) = size
	generateMaze(size)
	fillMaze()
	for i in range(0, 2*height+1):
		sys.stdout.write(str(maze[i])+'\n')
def sphereVertices(size):
	global R
	(n, m) = size
	l = []
	for i in range(0, m):
		for j in range(0, n):
			l.append([R*math.sin(math.pi/(m+1)*(i+1))*math.cos(2*math.pi/n*j), R*math.sin(math.pi/(m+1)*(i+1))*math.sin(2*math.pi/n*j), R*math.cos(math.pi/(m+1)*(i+1))])
	l.insert(0, [0, 0, R])
	for i in range(0, m):
		for j in range(0, n):
			l.append([(R+wallH)*math.sin(math.pi/(m+1)*(i+1))*math.cos(2*math.pi/n*j), (R+wallH)*math.sin(math.pi/(m+1)*(i+1))*math.sin(2*math.pi/n*j), (R+wallH)*math.cos(math.pi/(m+1)*(i+1))])
	l.append([0, 0, -R])
	return l
def sphereFaces(size):
	global wallX, wallY
	(n, m) = size
	#pole nord
	f = [[0, i, i+1] for i in range(1, n)]; f.append([0, n, 1])
	#pole sud
	f.extend([[2*n*m+1, i, i+1] for i in range(n*(m-1)+1, (n*m))]); f.append([2*n*m+1, n*(m-1)+1, n*m])
	#premiere couche
	for i in range(0, m-1):
		for j in range(1, n):
			f.append([n*i+j, n*i+j+1, n*(i+1)+j+1, n*(i+1)+j])	
		f.append([n*i+1, n*(i+1), n*(i+2), n*(i+1)+1])

	#wall
	offset = m*n
	for i in range(0, m-1):#height
		for j in range(0, n):#width
			if(wallX[j][i]):# wallX[width][height]
				f.append([i*n+j+1, offset+i*n+j+1, offset+(i+1)*n+j+1, (i+1)*n+j+1])
			if(wallY[i][j]):#wallY[height][width]
				f.append([i*n+j+1, offset+i*n+j+1, offset+i*n+((j+1)%n)+1, i*n+((j+1)%n)+1])
	offset = (2*m-1)*n
	for j in range(0, n):
		if(wallY[0][j]):
			f.append([(m-1)*n+j+1, offset+j+1, offset+((j+1)%n)+1, (m-1)*n+((j+1)%n)+1])
	return f	
if(blender):
	def generateMazeSphere(size, r=3, wH=0.5, context=bpy.context):
		'''Generate a Maze on a Sphere in blender 2.6, please use with blender otherwise, it will fail'''
		global width, height, wallX, wallY, R, wallH
		R = r; wallH = wH
		(width, height) = size
		generateMaze((width, height-1))
		mesh = bpy.data.meshes.new('MazeSphere')
		mesh.from_pydata(sphereVertices(size), [], sphereFaces(size))
		mesh.update()
		try:
			import bpy_extras
			bpy_extras.object_utils.object_data_add(context, mesh, operator=None)
		except ImportError:
			sys.stderr.write("Error with adding objects?\n")
	bpy.context.user_preferences.edit.use_global_undo = True 
#if __name__ == "__main__":
#	try:
#		generateMazeSphere((10, 10), 3, 0.5)
#	except Exception:
#		sys.stderr.write("You're not using blender 2.6, please use it\n")
