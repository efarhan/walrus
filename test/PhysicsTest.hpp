/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef WALRUS_PHYSICSTEST_H
#define WALRUS_PHYSICSTEST_H
#include<walrus/Physics.hpp>
#include<walrus/Square.hpp>

#include<cppunit/TestAssert.h>
#include<cppunit/Portability.h>
#include<cppunit/TestFixture.h>
#include<cppunit/TestCaller.h>
#include<cppunit/TestSuite.h>
#include<cppunit/TestCase.h>
#include<cppunit/TestRunner.h>

class PhysicsTest: public CppUnit::TestFixture
{
private:
	Physics *p;
public:
	static CppUnit::Test *suite()
	{
		CppUnit::TestSuite *suiteOfTests = new CppUnit::TestSuite(
				"PhysicsTest");
		suiteOfTests->addTest(
				new CppUnit::TestCaller<PhysicsTest>("testCollision",
						&PhysicsTest::testCollision));

		return suiteOfTests;
	}
	void setUp()
	{
		p = new PBPhysics();
	}
	void testCollision()
	{
		walrus::physics::Side flat_left = p->collisionSide(
				Square(Point(0, 0), Point(2, 2)),
				Square(Point(6, 0), Point(8, 2)),
				Square(Point(7, 0), Point(9, 2)));
		CPPUNIT_ASSERT(flat_left == walrus::physics::LEFT);

		walrus::physics::Side left_up = p->collisionSide(
				Square(Point(0, 0), Point(2, 2)),
				Square(Point(6, 6), Point(8, 8)),
				Square(Point(5, 0), Point(10, 9)));
		CPPUNIT_ASSERT(left_up == walrus::physics::LEFT);

		walrus::physics::Side left_down = p->collisionSide(
				Square(Point(0, 9), Point(2, 11)),
				Square(Point(6, 6), Point(8, 8)),
				Square(Point(5, 5), Point(10, 10)));
		CPPUNIT_ASSERT(left_down == walrus::physics::LEFT);

		walrus::physics::Side flat_right = p->collisionSide(
				Square(Point(4, 0), Point(6, 2)),
				Square(Point(1, 0), Point(3, 2)),
				Square(Point(0, 0), Point(2, 2)));
		CPPUNIT_ASSERT(flat_right == walrus::physics::RIGHT);

		walrus::physics::Side right_down = p->collisionSide(
				Square(Point(8, 8), Point(10, 11)),
				Square(Point(5, 4), Point(7, 7)),
				Square(Point(0, 0), Point(6, 10)));
		CPPUNIT_ASSERT(right_down == walrus::physics::RIGHT);

		walrus::physics::Side right_up = p->collisionSide(
				Square(Point(8, 0), Point(10, 2)),
				Square(Point(5, 5), Point(7, 7)),
				Square(Point(0, 0), Point(6, 10)));
		CPPUNIT_ASSERT(right_up == walrus::physics::RIGHT);

		walrus::physics::Side flat_down = p->collisionSide(
				Square(Point(0, 4), Point(2, 6)),
				Square(Point(0, 1), Point(2, 3)),
				Square(Point(0, 0), Point(2, 2)));
		CPPUNIT_ASSERT(flat_down == walrus::physics::DOWN);

		walrus::physics::Side down_left = p->collisionSide(
				Square(Point(0, 4), Point(2, 6)),
				Square(Point(1, 1), Point(3, 3)),
				Square(Point(2, 0), Point(4, 2)));
		CPPUNIT_ASSERT(down_left == walrus::physics::DOWN);

		walrus::physics::Side down_right = p->collisionSide(
				Square(Point(2, 4), Point(4, 6)),
				Square(Point(1, 1), Point(3, 3)),
				Square(Point(2, 0), Point(4, 2)));
		CPPUNIT_ASSERT(down_right == walrus::physics::DOWN);

		walrus::physics::Side up_right = p->collisionSide(
				Square(Point(2, 0), Point(4, 2)),
				Square(Point(1, 2), Point(3, 4)),
				Square(Point(0, 4), Point(2, 6)));
		CPPUNIT_ASSERT(up_right == walrus::physics::UP);

		walrus::physics::Side up_left = p->collisionSide(
				Square(Point(-1, 0), Point(1, 2)),
				Square(Point(1, 2), Point(3, 4)),
				Square(Point(0, 4), Point(2, 6)));
		CPPUNIT_ASSERT(up_left == walrus::physics::UP);

		walrus::physics::Side flat_up = p->collisionSide(
				Square(Point(0, 0), Point(2, 2)),
				Square(Point(0, 3), Point(2, 5)),
				Square(Point(0, 4), Point(2, 6)));
		CPPUNIT_ASSERT(flat_up == walrus::physics::UP);
		std::cout << "Collision diagonal UP:" << walrus::physics::UP
				<< " DOWN: " << walrus::physics::DOWN << " RIGHT: "
				<< walrus::physics::RIGHT << " LEFT: " << walrus::physics::LEFT
				<< "\n";

		walrus::physics::Side diagonal_up_left = p->collisionSide(
				Square(Point(0, 0), Point(2, 2)),
				Square(Point(2, 2), Point(4, 4)),
				Square(Point(1, 1), Point(3, 3)));
		std::cout << "Diagonal up left: " << diagonal_up_left << "\n";

		walrus::physics::Side diagonal_up_right = p->collisionSide(
				Square(Point(3, 0), Point(5, 2)),
				Square(Point(1, 2), Point(3, 4)),
				Square(Point(0, 3), Point(2, 5)));
		std::cout << "Diagonal up right: " << diagonal_up_right << "\n";

		walrus::physics::Side diagonal_down_right = p->collisionSide(
				Square(Point(3, 3), Point(5, 5)),
				Square(Point(1, 1), Point(3, 3)),
				Square(Point(0, 0), Point(2, 2)));
		std::cout << "Diagonal down right: " << diagonal_down_right << "\n";

		walrus::physics::Side diagonal_down_left = p->collisionSide(
				Square(Point(0, 3), Point(2, 5)),
				Square(Point(2, 1), Point(4, 3)),
				Square(Point(3, 0), Point(5, 2)));
		std::cout << "Diagonal down left: " << diagonal_down_left << "\n";
	}

	void tearDown()
	{
		delete p;

	}
};
#endif
