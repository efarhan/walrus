/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef WALRUS_POINTTEST_H
#define WALRUS_POINTTEST_H
#include<walrus/Point.hpp>

#include<cppunit/TestAssert.h>
#include<cppunit/Portability.h>
#include<cppunit/TestFixture.h>
#include<cppunit/TestCaller.h>
#include<cppunit/TestSuite.h>
#include<cppunit/TestCase.h>
#include<cppunit/TestRunner.h>

class PointTest: public CppUnit::TestFixture
{
private:
	Point *p1, *p2, *p3;
public:
	static CppUnit::Test *suite()
	{
		CppUnit::TestSuite *suiteOfTests = new CppUnit::TestSuite("PointTest");
		suiteOfTests->addTest(
				new CppUnit::TestCaller<PointTest>("testMinus",
						&PointTest::testMinus));
		suiteOfTests->addTest(
				new CppUnit::TestCaller<PointTest>("testCompare",
						&PointTest::testCompare));

		return suiteOfTests;
	}
	void setUp()
	{
		p1 = new Point(1, 1);
		p2 = new Point(10, 1);
		p3 = new Point(-2, -3);
	}
	void testMinus()
	{
		Point p = *(this->p2) - *(this->p1);
		CPPUNIT_ASSERT(p == Point(9, 0));
		p = *(p2) - *(p3);
		CPPUNIT_ASSERT(p == Point(12, 4));
		CPPUNIT_ASSERT(Point(8, 8) - Point(5, 4) == Point(3, 4));
	}
	void testCompare()
	{
		CPPUNIT_ASSERT((*p1) < (*p2));
		CPPUNIT_ASSERT((*p1) < (*p3));
		CPPUNIT_ASSERT((*p2) > (*p3));
		;

	}
	void tearDown()
	{
		delete p1;
		delete p2;
		delete p3;
	}
};
#endif
