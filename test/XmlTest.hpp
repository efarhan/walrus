/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WALRUS_XMLTEST_HPP_
#define WALRUS_XMLTEST_HPP_

#include<walrus/XmlReader.hpp>
#include<walrus/XmlWrite.hpp>

#include<cppunit/TestAssert.h>
#include<cppunit/Portability.h>
#include<cppunit/TestFixture.h>
#include<cppunit/TestCaller.h>
#include<cppunit/TestSuite.h>
#include<cppunit/TestCase.h>
#include<cppunit/TestRunner.h>

class XmlTest: public CppUnit::TestFixture
{
private:
	Scene* scene;
	Level* level;
	Character* player;
public:
	static CppUnit::Test *suite()
	{
		CppUnit::TestSuite *suiteOfTests = new CppUnit::TestSuite("XmlTest");
		suiteOfTests->addTest(
				new CppUnit::TestCaller<XmlTest>("testReadScene",
						&XmlTest::testRead));
		suiteOfTests->addTest(
				new CppUnit::TestCaller<XmlTest>("testReadPlayer",
						&XmlTest::testReadPlayer));
		suiteOfTests->addTest(
				new CppUnit::TestCaller<XmlTest>("testWrite",
						&XmlTest::testWrite));
		return suiteOfTests;
	}
	void setUp()
	{
		scene = NULL;
		level = NULL;
		player = NULL;
	}
	void testRead()
	{

		scene = readScene("testScene.xml");
		CPPUNIT_ASSERT(scene != NULL);

	}
	void testReadPlayer()
	{
		player = readCharacter("testPlayer.xml", Point(0, 0));
		CPPUNIT_ASSERT(player != NULL);
		int* imgIndex = player->getImgIndex();
		std::cout << "\nImgIndexes : ";
		for (int i = 0; i < walrus::anim::STATE; i++)
			std::cout << "," << imgIndex[i] << ",";
		std::cout << "\n";

	}
	void testWrite()
	{

		CPPUNIT_ASSERT(!writeScene(scene));
		CPPUNIT_ASSERT(!writeLevel(level));
		CPPUNIT_ASSERT(!writeCharacter(player));

	}
	void tearDown()
	{
		delete scene;
		delete level;
		delete player;
	}
};

#endif /* XMLTEST_HPP_ */
