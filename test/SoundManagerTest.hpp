/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WALRUS_SOUNDMANAGERTEST_HPP_
#define WALRUS_SOUNDMANAGERTEST_HPP_
#include<utility/SoundManager.hpp>

#include<cppunit/TestAssert.h>
#include<cppunit/Portability.h>
#include<cppunit/TestFixture.h>
#include<cppunit/TestCaller.h>
#include<cppunit/TestSuite.h>
#include<cppunit/TestCase.h>
#include<cppunit/TestRunner.h>

class SoundManagerTest: public CppUnit::TestFixture
{
private:
	SoundManager* manager;
public:
	static CppUnit::Test *suite()
	{
		CppUnit::TestSuite *suiteOfTests = new CppUnit::TestSuite(
				"SoundManagerTest");
		suiteOfTests->addTest(
				new CppUnit::TestCaller<SoundManagerTest>("testSound",
						&SoundManagerTest::testSound));
		suiteOfTests->addTest(
				new CppUnit::TestCaller<SoundManagerTest>("testMusic",
						&SoundManagerTest::testMusic));
		return suiteOfTests;
	}
	void setUp()
	{
		manager = new SoundManager();
	}
	void testSound()
	{

	}
	void testMusic()
	{

	}
	void tearDown()
	{
		delete manager;
	}
};

#endif /* SOUNDMANAGERTEST_HPP_ */
