/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef WALRUS_SQUARETEST_H
#define WALRUS_SQUARETEST_H
#include<walrus/Square.hpp>

#include<cppunit/TestAssert.h>
#include<cppunit/Portability.h>
#include<cppunit/TestFixture.h>
#include<cppunit/TestCaller.h>
#include<cppunit/TestSuite.h>
#include<cppunit/TestCase.h>
#include<cppunit/TestRunner.h>

class SquareTest: public CppUnit::TestFixture
{
private:
	Square* s;
public:
	static CppUnit::Test *suite()
	{
		CppUnit::TestSuite *suiteOfTests = new CppUnit::TestSuite("SquareTest");
		suiteOfTests->addTest(
				new CppUnit::TestCaller<SquareTest>("testCheckPoints",
						&SquareTest::testCheckPoints));
		suiteOfTests->addTest(
				new CppUnit::TestCaller<SquareTest>("testPointIsIn",
						&SquareTest::testPointIsIn));
		suiteOfTests->addTest(
				new CppUnit::TestCaller<SquareTest>("testSquareIsIn",
						&SquareTest::testSquareIsIn));
		return suiteOfTests;
	}
	void setUp()
	{
		s = new Square(Point(1, 1), Point(3, 3));
	}
	void testCheckPoints()
	{
		Square s1 = Square(Point(3, 3), Point(1, 1));
		CPPUNIT_ASSERT(s1 == *s);
	}
	void testPointIsIn()
	{
		CPPUNIT_ASSERT(s->isIn(Point(2, 2)));
	}
	void testSquareIsIn()
	{
		Square s1 = Square(Point(2, 2), Point(4, 4));
		CPPUNIT_ASSERT(s->isIn(s1));

		s1 = Square(Point(2, 2), Point(-4, 4));
		CPPUNIT_ASSERT(s->isIn(s1));

		s1 = Square(Point(0, 1), Point(3, 2));
		Square s2 = Square(Point(1, 0), Point(2, 3));
		CPPUNIT_ASSERT(s1.isIn(s2));

		s1 = Square(Point(4, 4), Point(5, 5));
		CPPUNIT_ASSERT(!s->isIn(s1));

		s1 = Square(Point(0, 2), Point(2, 4));
		CPPUNIT_ASSERT(s->isIn(s1));

		CPPUNIT_ASSERT(
				!Square(Point(133, 100), Point(165, 132)).isIn(
						Square(Point(100, 35), Point(164, 99))));

	}
	void tearDown()
	{
		delete s;
	}
};
#endif
