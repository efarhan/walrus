#include "PythonTest.hpp"
#include "PointTest.hpp"
#include "XmlTest.hpp"
#include "GameEngineTest.hpp"
#include "SoundManagerTest.hpp"
#include "SquareTest.hpp"
#include "PhysicsTest.hpp"

#include <cppunit/ui/text/TestRunner.h>

int main(int argc, char **argv)
{
	(void) argc;
	(void) argv;
	CppUnit::TextUi::TestRunner runner;
	runner.addTest(PointTest::suite());
	runner.addTest(SquareTest::suite());
	runner.addTest(PhysicsTest::suite());
	runner.addTest(XmlTest::suite());
	runner.addTest(PythonTest::suite());
	runner.addTest(GameEngineTest::suite());
	runner.addTest(SoundManagerTest::suite());
	runner.run();
#ifdef WIN32
	while(1);
#endif
	return 0;
}
