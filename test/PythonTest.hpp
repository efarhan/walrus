/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PYTHONTEST_HPP_
#define PYTHONTEST_HPP_
#include <utility/PythonInterface.hpp>
#include <walrus/Engine.hpp>
#include <walrus/SFMLEngine.hpp>

#include <cppunit/TestAssert.h>
#include <cppunit/Portability.h>
#include <cppunit/TestFixture.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestRunner.h>

class PythonTest: public CppUnit::TestFixture
{
private:
	PythonInterface* p;
	Engine * engine;
public:
	static CppUnit::Test *suite()
	{
		CppUnit::TestSuite *suiteOfTests = new CppUnit::TestSuite("PythonTest");
		suiteOfTests->addTest(
				new CppUnit::TestCaller<PythonTest>("testPython",
						&PythonTest::testPython));
		return suiteOfTests;
	}
	void setUp()
	{
		p = new PythonInterface(NULL);
		engine = new SFMLEngine();
	}
	void testPython()
	{
		CPPUNIT_ASSERT(p->init() != 1);
		CPPUNIT_ASSERT(p->execute(std::string("testPython.py")) != 1);
		CPPUNIT_ASSERT(p->execute(std::string("unfound.py")) == 1);
		CPPUNIT_ASSERT(engine->execute(std::string("testGetEngine.py")) != 1);

	}
	void tearDown()
	{
		delete p;
		delete engine;
	}
};

#endif /* PYTHONTEST_HPP_ */
