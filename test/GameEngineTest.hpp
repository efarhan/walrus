/*
 Walrus open project
 Copyright (C) 2012  Elias Aran Paul Farhan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WALRUS_GAMEENGINETEST_H_
#define WALRUS_GAMEENGINETEST_H_

#include<walrus/SFMLEngine.hpp>
#include<walrus/Engine.hpp>
#include<utility/Event.hpp>

#include<cppunit/TestAssert.h>
#include<cppunit/Portability.h>
#include<cppunit/TestFixture.h>
#include<cppunit/TestCaller.h>
#include<cppunit/TestSuite.h>
#include<cppunit/TestCase.h>
#include<cppunit/TestRunner.h>

class GameEngineTest: public CppUnit::TestFixture
{
private:
	Engine* engine;
	walrus::event::Event* quit;
public:
	static CppUnit::Test *suite()
	{
		CppUnit::TestSuite *suiteOfTests = new CppUnit::TestSuite(
				"GameEngineTest");
		suiteOfTests->addTest(
				new CppUnit::TestCaller<GameEngineTest>("testBackgroundOnly",
						&GameEngineTest::testBackgroundOnly));
		suiteOfTests->addTest(
				new CppUnit::TestCaller<GameEngineTest>("testPlayerWithPhysics",
						&GameEngineTest::testPlayerWithPhysics));
		return suiteOfTests;
	}
	void setUp()
	{
		engine = NULL;
		quit = NULL;
	}
	void testBackgroundOnly()
	{
		engine = new SFMLEngine();
		engine->init();
		engine->config();
		bool success = false;
		success = engine->setScene1("testScene.xml");

		engine->switchScene();
		if (success)
		{
			
			quit = new walrus::event::SpecificEvent<Engine>();
			((walrus::event::SpecificEvent<Engine>*)quit)->setFunction(engine, &Engine::quit, 2.0);
			engine->addEvent(quit);
			engine->run();
		}
		else
		{
			std::cerr
					<< "Error in testBackgroundOnly, unable to load testScene.xml\n";
			CPPUNIT_ASSERT(success);
		}
	}
	void testPlayerWithPhysics()
	{
		engine = new SFMLEngine();
		engine->init();
		engine->config();
		bool success = false;
		success = engine->setScene1("testGame1.xml");

		engine->switchScene();
		if (success)
		{
			quit = new walrus::event::SpecificEvent<Engine>();
			((walrus::event::SpecificEvent<Engine>*)quit)->setFunction(engine, &Engine::quit, 2.0);
			engine->addEvent(quit);
			engine->run();
		}
		else
			CPPUNIT_ASSERT(success);
	}
	void tearDown()
	{
		delete engine;
		delete quit;
	}
};

#endif /* GAMEENGINETEST_H_ */
