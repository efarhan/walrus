cmake_minimum_required (VERSION 2.6)
project (Walrus)
# The version number.
set (Tutorial_VERSION_MAJOR 0)
set (Tutorial_VERSION_MINOR 1)



#definitions
set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake_modules" ${CMAKE_MODULE_PATH})

find_package(CppUnit)
#can be use : system, graphics, network
find_package(SFML COMPONENTS system window audio REQUIRED)
#Horror for PNG in Windows
find_package(PNG)
if(PNG_FOUND)
	find_package(PNG REQUIRED)
else(PNG_FOUND)
	find_package(ZLIB REQUIRED)
	if(ZLIB_FOUND)
		#For Windows GnuWin32
		set(PNG_PNG_INCLUDE_DIR ${ZLIB_INCLUDE_DIR})
		set(PNG_NAMES ${PNG_NAMES} png libpng png15 libpng15 png15d libpng15d png14 libpng14 png14d libpng14d png12 libpng12 png12d libpng12d)
		
		set(_ZLIB_SEARCH_NORMAL "[HKEY_LOCAL_MACHINE\\SOFTWARE\\GnuWin32\\Zlib;InstallPath]")
		find_library(PNG_LIBRARY  NAMES ${PNG_NAMES} PATHS ${PNG_PNG_INCLUDE_DIR}/../ PATH_SUFFIXES lib)
		find_library(PNG_LIBRARY NAMES ${PNG_NAMES} )
		mark_as_advanced(PNG_LIBRARY PNG_PNG_INCLUDE_DIR)
		
		if (PNG_LIBRARY AND PNG_PNG_INCLUDE_DIR)
		# png.h includes zlib.h. Sigh.
			MESSAGE("PNG FOUND")
			set(PNG_INCLUDE_DIRS ${PNG_PNG_INCLUDE_DIR} ${ZLIB_INCLUDE_DIR} )
			set(PNG_INCLUDE_DIR ${PNG_INCLUDE_DIRS} ) # for backward compatiblity
			set(PNG_LIBRARIES ${PNG_LIBRARY} ${ZLIB_LIBRARY})
		elseif(PNG_PNG_INCLUDE_DIR)
			MESSAGE("PNG LIBRARY NOT FOUND")
		else()
			MESSAGE("PNG NOT FOUND")
		endif()
		if (PNG_PNG_INCLUDE_DIR AND EXISTS "${PNG_PNG_INCLUDE_DIR}/png.h")
			file(STRINGS "${PNG_PNG_INCLUDE_DIR}/png.h" png_version_str REGEX "^#define[ \t]+PNG_LIBPNG_VER_STRING[ \t]+\".+\"")

			string(REGEX REPLACE "^#define[ \t]+PNG_LIBPNG_VER_STRING[ \t]+\"([^\"]+)\".*" "\\1" PNG_VERSION_STRING "${png_version_str}")
			unset(png_version_str)
		endif ()
	else(ZLIB_FOUND)
		MESSAGE("ZLIB NOT FOUND")
	endif(ZLIB_FOUND)
endif(PNG_FOUND)
add_definitions(${PNG_CFLAGS})
add_definitions(${OPENGL_CFLAGS})
#includes
include(FindOpenGL)
include(FindPythonLibs)
include_directories(${PYTHON_INCLUDE_DIRS})
add_definitions(${PYTHON_CFLAGS})
include_directories(${SFML_INCLUDE_DIR})
if(CPPUNIT_FOUND)
	include_directories(${CPPUNIT_INCLUDE_DIR})
else()
	MESSAGE("CppUnit not found, the test will notbe added")
endif()
include_directories(${PNG_INCLUDE_DIR})
if(APPLE)
	add_definitions(-DAPPLE)
	#MacPorts
	include_directories(/opt/local/include)
	link_directories(/opt/local/lib)
endif(APPLE)
#set up build
set(CMAKE_SOURCE_DIR src)
include_directories(${CMAKE_SOURCE_DIR}/include)
FILE(GLOB_RECURSE CFILES src/*.c)
FILE(GLOB_RECURSE CPPFILES src/*/*.cpp)
FILE(GLOB MAIN src/walrus.cpp)
FILE(GLOB_RECURSE HFILES src/*.h)
FILE(GLOB_RECURSE HPPFILES src/*.hpp)
FILE(GLOB_RECURSE TESTFILES test/*)
FILE(GLOB_RECURSE EDITOR editor/*)
#Finalize executables
if(UNIX)
	add_definitions(-Wall -Wextra -g -O0)
endif(UNIX)

add_executable(Walrus ${MAIN} ${CPPFILES} ${CFILES} ${HFILES} ${HPPFILES} )

if(CPPUNIT_FOUND)
	add_executable(WalrusTest ${TESTFILES} ${CPPFILES} ${CFILES} ${HFILES} ${HPPFILES})
endif()
add_executable(WalrusEditor ${EDITOR} ${CPPFILES} ${CFILES} ${HFILES} ${HPPFILES})

set(DEPENDS ${OPENGL_LIBRARIES} ${SFML_LIBRARIES} ${PNG_LIBRARIES} ${PYTHON_LIBRARY} )
if(UNIX)
set(DEPENDS m ${DEPENDS})
endif(UNIX)
target_link_libraries(Walrus ${DEPENDS})
if(CPPUNIT_FOUND)
target_link_libraries(WalrusTest ${CPPUNIT_LIBRARIES} ${DEPENDS})
endif()
target_link_libraries(WalrusEditor ${DEPENDS})

